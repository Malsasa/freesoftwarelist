# Free Software List

List of free software that is easy to read for everybody. I make this for my students in [my computer teaching work](https://kursusteknoplasma.wordpress.com) in Indonesia. I teach computing exclusively with Free Software applications with Kubuntu operating system.

### Table of Contents
- [Document Editor](#document-editor)
- [Picture Viewer](#pictures-camera-and-photography)
- [Education](#education)
- [Communication](#communication-and-internet)
- [Graphic Design](#graphic-design)
- [Multimedia Player](#multimedia-player)
- [Multimedia Production](#multimedia-production)
- [Utility](#utility)
- [Programming](#programming)
- [Operating System](#operating-system)

# Document Editor 

| Free Software | Description |
|---|---|
| [LibreOffice](https://libreoffice) | Office suite |
| [Writer](https://libreoffice.org/writer) | Word processor, part of LibreOffice. |
| [Calc](https://libreoffice.org/calc) | Spreadsheet, part of LibreOffice. |
| [Impress](https://libreoffice.org/impress) | Presentation, part of LibreOffice. |
| [Base](https://libreoffice.org/base) | Database, part of LibreOffice.|
| [Draw](https://libreoffice.org/draw) | Drawing diagrams and edit PDF, part of LibreOffice. |
| [Math](https://libreoffice.org/math) | Equation editor, part of LibreOffice. | 
| PDF Viewer <br> ([Evince](https://wiki.gnome.org/Apps/Evince), [Okular](https://okular.kde.org), [SumatraPDF](https://www.sumatrapdfreader.org/free-pdf-reader.html)) | PDF reader, to read pdf documents. |
| [PDF Shuffler](https://sourceforge.net/projects/pdfshuffler) | PDF editor specifically to split, merge and rearrange. |
| [Scribus](https://scribus.net) | Desktop publishing, to design page layout and prepare printing. |

# Pictures, Camera and Photography

| Free Software | Nonfree Software | 
| -------- | -------- |
| [Image Viewer](https://help.gnome.org/users/eog/stable/)          |      Image viewer, built-in Trisquel.     |
| [Gwenview](https://userbase.kde.org/Gwenview)              |     Image viewer, built-in Kubuntu.          |
| [Nomacs](https://nomacs.org) | Image viewer, fast and cross platform. |
| [Darktable](https://www.darktable.org)     | Raw photo processor.     |
| [Rawtherapee](https://rawtherapee.com)     | Raw photo processor, alternative to Darktable.     |
| [F-Spot](https://help.gnome.org/users/f-spot/unstable/)     | Camera manager and image viewer.     |
| [GMIC](https://gmic.eu)     | Image Filters and processing framework.     |
| [Digikam](https://www.digikam.org)     | Professional photo and camera management.     |
| [Shotwell](https://wiki.gnome.org/Apps/Shotwell) | Image viewer and photo manager. |

# Education

| Free Software | Description |
| --- | --- |
| [LibreOffice](https://libreoffice.org) | Office suite, to write/open documents, spreadsheets, presentations, <br>and draw illustrations. Able to save as PDF. Pairable with StarDict and Zotero. | 
| [StarDict](http://stardict.sourceforge.net) | Desktop dictionary, to translate multiple language word by word <br>and speak it out loud for student. |
| [Zotero](https://zotero.org) | Reference manager, to save web pages and other references, and search <br>in them offline, and make citation, bibliography, footnote automatically. Pairable with LibreOffice. |

# Graphic Design

| Free Software | Description | 
| -------- | -------- |
|  [Akira](https://github.com/akiraux/Akira)    | Graphical user interface (GUI) designer. |
|  [Blender](https://blender.org)     | 3D modeller and animation maker, video editor and compositing tool.  |
|  [Darktable](darktable.org)     | Raw photos processor.     |
|  [Digikam](digikam.org)     | Camera manager.     |
|  [Inkscape](https://inkscape.org)     | Illustration editor (vector editor), to create any design like logos, posters, products, etc.    |
|  [GIMP](https://gimp.org)     | Image editor (bitmap editor), to manipulate pictures and photos as well as do digital painting.     |
|  [Krita](https://krita.org) | Digital painting tool, to make digital paintings, comics, mangas, and 2D animation. |
|  [Godot](https://godotengine.org)     | Game engine, to make 2D and 3D video games capable to run on phones and computers.     |
|  [Kdenlive](https://kdenlive.org)     | Video editor, to manipulate, cut, merge, remix videos, to make movies.  |
|  [Natron](https://natrongithub.github.io)     | Video compositing, to create visual effects in video.    |
|  [Rawtherapee](rawtherapee.com)     | Raw photos processor, alternative to Darktable.     |
|  [Shotwell](https://wiki.gnome.org/Apps/Shotwell)     | Image viewer, with camera management and crop features.     |
|  [Shutter](https://shutter-project.org)     | Screenshot tool, with full editing abilities.     |
|  [Scribus](https://scribus.net)     | Desktop publishing, to layout and print newspapers, books, magazines, posters, etc.     |
|  [Simple Scan](https://gitlab.gnome.org/GNOME/simple-scan)     | Scanning tool, to enable scanner devices.     |
|  [Synfig Studio](https://www.synfig.org)     | Animation maker, to create 2D animation movie. |

# Multimedia Player

| Free Software | Nonfree Software | 
| -------- | -------- |
| [Elisa](https://elisa.kde.org)     | Audio player, built-in Kubuntu.   |
| [Rhythmbox](https://wiki.gnome.org/Apps/Rhythmbox) | Audio player, built-in Trisquel. |
| [VLC](https://videolan.org) | Audio video player, cross platform and the most popular. |

# Communication and Internet

| Free Software | Description | 
|---|---|
| [Firefox](https://mozilla.org) | Web browser, to explore the internet and world wide web. |
| [Thunderbird](https://thunderbird.net) | Email client, to access your email inbox and send mails. |
| [Telegram](https://telegram.org) | Instant messaging, complete and convenient. |
| [Konversation](https://konversation.kde.org) | Chat program, to chat (traditionally) with the internet community. |
| [Jitsi](https://jitsi.org) | Video call, complete and convenient. |
| [Element](https://element.io) | Complete suite of communication and file sharing. |
| [Nextcloud](https://nextcloud.com) | File storage and sharing, complete and convenient. |
| [Persepolis](https://persepolisdm.github.io) | Download manager, integrated with web browser. |
| [Transmission](transmissionbt.com) | BitTorrent downloader, cross platform. |

# Multimedia Production

| Free Software | Nonfree Software | 
| -------- | -------- |
| [Audacity](https://www.audacityteam.org)   | Audio editor.     |
| [Blender](https://blender.org) | Complete suite of 3D graphic editor, video editor <br>and compositor, and animation maker. |
| [Kdenlive](https://kdenlive.org)     | Video editor.     |
| [Pitivi](http://www.pitivi.org)     | Video editor, alternative to Kdenlive.     |
| [Natron](natrongithub.github.io)     | Video compositor.     |
| [Cinelerra](https://www.cinelerra-gg.org)     | Video editor, superior to Kdenlive.     |
| [Synfig Studio](synfig.org)     | Animation maker.     |
| [Tupi](https://sourceforge.net/projects/tupi2d)     | Animation maker, alternative to Synfig.     |
| [Enve](https://maurycyliebner.github.io)     | Animation maker, alternative to Synfig.     |

# Utility

| Free Software | Description |
| --- | --- |
| [Unetbootin](https://unetbootin.github.io) | USB flash disk bootable maker, to write iso image to install operating system. |
| [GParted](https://gparted.org) | Partition editor, to format and resize hard disk drive storage. |
| [Synaptic](https://www.nongnu.org/synaptic) | Package manager, to add/remove programs into your computer. |
| [KeePassXC](https://keepassxc.org) | Password manager, to save your login secrets safely. |
| [PeaZip](https://peazip.github.io) | Archive manager, to create compressed archive files for easier sharing. <br>Able to protect archive files with passwords. PeaZip supports A-Z archive formats. |
| [KDE Connect](https://kdeconnect.kde.org) | Phone and computer connector, to share files quickly between your devices. |

# Programming

| Free Software | Nonfree Software |
| ---|--- |
| [Geany](https://geany.org)      | Code text editor, to write program in any language. Geany supports A-Z languages. |
| [GCC](https://gcc.gnu.org) | GNU Compiler Collection, able to compile C and C++, part of GNU Operating System. |
| [GDB](https://www.gnu.org/s/gdb) | GNU Debugger, to fix program errors. | 
| [Qt](https://qt.io)      | Complete software development solution. A complete replacement to Microsoft's Frameworks<br> such as Visual Basic and .Net Framework.      |
| [Creator](https://www.qt.io/product/development-tools) | Integrated development environment (IDE), part of Qt. |
| [Designer](https://doc.qt.io/qt-5/qtdesigner-manual.html) | Graphical user interface (GUI) designer, part of Qt. |
| [Linguist](https://doc.qt.io/qt-5/qtlinguist-index.html) | Internationalization (i18n) editor, part of Qt. |

# Electronics

| Free Software | Description |
| --- | --- | 
| [KiCAD](https://kicad.org) | Schematics design and PCB maker. |
| [GEDA](http://geda-project.org) | Electronics Design Automation, full suite including simulation. |
| [LibrePCB](https://librepcb.org) | Schematics design, PCB maker, alternative to KiCAD and GEDA. | 
| [NGSPICE](http://ngspice.sourceforge.net) | World's most popular electronic simulator. |

# Operating System 

| Free Software | Description |
| --- | --- |
| [Kubuntu](https://kubuntu.org) | Free operating system, the excellent replacement to Microsoft Windows and Apple MacOS. Mass produced already as high performance laptops. Easy to learn, easy to teach. Proven as students' operating systems of Teknoplasma computer course. |
| [Trisquel](https://trisquel.info) | Free operating system, great for laptops perfect for PCs. Mass produced already as Lenovo ThinkPad based Respects Your Freedom laptops. Unlike Kubuntu, it is fully free software. It is the operating system for anyone ready to achieve software freedom after learning computing with Kubuntu. |