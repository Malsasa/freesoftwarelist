# List of Commercial Free Software Applications

The purpose of this list is to provide reference information about commercial free-software applications. To clarify, the category is not a contradiction as the free software intended here is libre software and not gratisware by definition and following English language rules, the addition of the word commercial made it means libre software that is sold in commercial way by the developers. Most other parties e.g. Wikipedia prefer to call this category commercial open-source applications but here the naming open source is not used and instead free software is used as an education.

Red Hat Enterprise Linux
SUSE Linux Enterprise
elementary OS
UOS
Zorin OS 
